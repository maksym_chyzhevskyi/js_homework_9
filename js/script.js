/* Опишіть, як можна створити новий HTML тег на сторінці.
За допомогою методу createElement(tag) створюємо новий елемент з
відповідним тегом та додаємо його на сторінку за допомогою відповідних 
методів, в залежності від того, де нам потрібно його розташувати:
1. append - в кінець ноди
2. prepend - на початок ноди
3. before - до початку ноди
4. after - після початку ноди
5. replaceWith - здійснює заміну ноди
Також можна використати метод insertAdjacentHTML(position, text), вказавши як position місце розташування тегу,
замість text - який саме тег та його наповнення.

Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр визначає місце розташування нового елементу по відношенню до елементу, яким був
викликаний метод insertAdjacentHTML.
Варіанти першого параметру:
1. beforebegin - новий елемент буде розташовано до елементу, яким був
   викликаний метод insertAdjacentHTML
2. afterbegin - новий елемент буде розташовано одразу після відкриваючого тегу елементу, яким був
   викликаний метод insertAdjacentHTML
3. beforeend - новий елемент буде розташовано до закриваючого тегу елементу, яким був
   викликаний метод insertAdjacentHTML
4. afterend - новий елемент буде розташовано після елементу, яким був
   викликаний метод insertAdjacentHTML

Як можна видалити елемент зі сторінки?
За допомогою методу remove.
*/

function getPageList(arr, parent = document.body) {
    arr = arr.reverse(arr);
    let mainElement = document.createElement('ol');
    parent.prepend(mainElement);
    for (let i of arr) {
        let listItem = `<li>${i}</li>`;
        mainElement.insertAdjacentHTML('afterbegin', listItem)
    }
}

const arrayOne = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];

let optionalArg;

getPageList(arrayOne, optionalArg);








